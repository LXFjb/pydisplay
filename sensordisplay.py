#!/usr/bin/env python

import os
import board
from digitalio import DigitalInOut
from PIL import Image, ImageDraw, ImageFont
import adafruit_rgb_display.st7789 as st7789

import grovepi
import time
#import random
import smbus

import csv

# Create the display
cs_pin = DigitalInOut(board.CE0)
dc_pin = DigitalInOut(board.D25)
reset_pin = DigitalInOut(board.D24)
BAUDRATE = 24000000

spi = board.SPI()
disp = st7789.ST7789(
    spi,
    height=240,
    y_offset=80,
    rotation=0,
    cs=cs_pin,
    dc=dc_pin,
    rst=reset_pin,
    baudrate=BAUDRATE,
)

backlight = DigitalInOut(board.D26)
backlight.switch_to_output()
backlight.value = True

width = disp.width
height = disp.height
image = Image.new("RGB", (width,height))
draw = ImageDraw.Draw(image)

bus = smbus.SMBus(1)
fnt = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf", 30)
lastupdate = 0
moist = 0
qual = 0
light = 0
        
def updateth():

    bus.write_byte_data(0x40, 0x03, 0x11)
    time.sleep(0.5)

    # TH02 address, 0x40(64)
    # Read data back from 0x00(00), 3 bytes
    # Status register, cTemp MSB, cTemp LSB
    
    data = bus.read_i2c_block_data(0x40, 0x00, 3)
    # Convert the data to 14-bits
    cTemp = ((data[1] * 256 + (data[2] & 0xFC))/ 4.0) / 32.0 - 50.0
    #fTemp = cTemp * 1.8 + 32
    
    bus.write_byte_data(0x40, 0x03, 0x01)
    time.sleep(0.5)
    data = bus.read_i2c_block_data(0x40, 0x00, 3)
    
    # Convert the data to 12-bits
    humidity = ((data[1] * 256 + (data[2] & 0xF0)) / 16.0) / 16.0 - 24.0
    humidity = humidity - (((humidity * humidity) * (-0.00393)) + (humidity * 0.4008) - 4.7844)
    humidity = humidity + (cTemp - 30) * (humidity * 0.00237 + 0.1973)
    
    return(cTemp, humidity)

def checkGraph():
    if os.path.exists("temp.png"):
            graph = Image.open("temp.png")
            image.paste(graph)

checkGraph()

while True:
    draw.rectangle((0, 0, width, 110), outline=0, fill=(0, 0, 0))
    th = updateth()
    temp = th[0]
    hum = th[1]
    
    #time.sleep(1)
    #moist = grovepi.analogRead(0)
    #time.sleep(1)
    #qual = grovepi.analogRead(1)
    #time.sleep(1)
    #light = grovepi.analogRead(2)
    timestamp = int(time.time())
    if timestamp - lastupdate > 300:
        lastupdate = timestamp
        datafile = open('temp.csv','a')
        with datafile:
            writer = csv.writer(datafile)
            writer.writerow([timestamp, temp])
        datafile.close()
        checkGraph()
        
    displaystr = "{:.2f},{:.2f}".format(temp,hum)
    tempstr="{:.1f}°C".format(temp)
    humstr="{:.2f}%rH".format(hum)

    draw.text((20,10), tempstr, font=fnt)
    draw.text((20,40), humstr, font=fnt) 
    disp.image(image)
    time.sleep(5)
