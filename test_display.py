#!/usr/bin/env python

import os
import board
from digitalio import DigitalInOut
from PIL import Image, ImageDraw, ImageFont
import adafruit_rgb_display.st7789 as st7789


# Create the display
cs_pin = DigitalInOut(board.CE0)
dc_pin = DigitalInOut(board.D25)
reset_pin = DigitalInOut(board.D24)
BAUDRATE = 24000000

spi = board.SPI()
disp = st7789.ST7789(
    spi,
    height=240,
    y_offset=80,
    rotation=0,
    cs=cs_pin,
    dc=dc_pin,
    rst=reset_pin,
    baudrate=BAUDRATE,
)

backlight = DigitalInOut(board.D26)
backlight.switch_to_output()
backlight.value = True

width = disp.width
height = disp.height
image = Image.new("RGB", (width,height))
draw = ImageDraw.Draw(image)

draw.rectangle((0, 0, width, height), outline=0, fill=(0, 0, 0))

#use your own png file here
graphic = Image.open("lxf.png")
image.paste(graphic)
fnt = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf", 30)
draw.text ((20,120), "Hello world", font=fnt)
disp.image(image)
