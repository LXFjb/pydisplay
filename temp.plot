#temp.plot

set terminal pngcairo size 240,240 background '#000000'
set border lw 0.5 lc '#959595'
set output 'temp.png'

set lmargin screen 0.15
set bmargin screen 0.25
set tmargin screen 0.5
set rmargin screen 0.95
set key off

set datafile separator ','
set title "Temp"
#set yrange [12:30]
set ylabel ""
set ytics 2
#set y2tics 100
set xdata time
set timefmt "%s"
set format x "%H"
#set format x ""
#set format y ""
set xtics 3600
#set xtics rotate

plot "< tail -n 500 temp.csv" using 1:2 with lines axes x1y1

#while (1) {
#    pause 5
#    replot
#}
