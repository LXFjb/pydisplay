#!/usr/bin/env python

import grovepi
import time
#import random
import smbus

import csv
from datetime import datetime


bus = smbus.SMBus(1)
        
def update():

    bus.write_byte_data(0x40, 0x03, 0x11)
    time.sleep(0.5)

    # TH02 address, 0x40(64)
    # Read data back from 0x00(00), 3 bytes
    # Status register, cTemp MSB, cTemp LSB
    
    data = bus.read_i2c_block_data(0x40, 0x00, 3)
    # Convert the data to 14-bits
    cTemp = ((data[1] * 256 + (data[2] & 0xFC))/ 4.0) / 32.0 - 50.0
    #fTemp = cTemp * 1.8 + 32
    
    bus.write_byte_data(0x40, 0x03, 0x01)
    time.sleep(0.5)
    data = bus.read_i2c_block_data(0x40, 0x00, 3)
    
    # Convert the data to 12-bits
    humidity = ((data[1] * 256 + (data[2] & 0xF0)) / 16.0) / 16.0 - 24.0
    humidity = humidity - (((humidity * humidity) * (-0.00393)) + (humidity * 0.4008) - 4.7844)
    humidity = humidity + (cTemp - 30) * (humidity * 0.00237 + 0.1973)
    
    return(cTemp, humidity)


while True:
    th = update()
    temp = th[0]
    hum = th[1]
    
    time.sleep(1)
    moist = grovepi.analogRead(0)
    time.sleep(1)
    qual = grovepi.analogRead(1)
    time.sleep(1)
    light = grovepi.analogRead(2)
    
    print (f"{temp:.2f}",f"{hum:.2f}",moist,qual,light)



